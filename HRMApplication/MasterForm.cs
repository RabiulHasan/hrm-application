﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace HRMApplication
{
    public partial class MasterForm : Form
    {
        public MasterForm()
        {
            InitializeComponent();
        }

        private void employeeInfoToolStripMenuItem_Click(object sender, EventArgs e)
        {
            EmployeeRegistration eform = new EmployeeRegistration();
            eform.Show();
        }

        private void employeeListToolStripMenuItem_Click(object sender, EventArgs e)
        {
            printReport pr = new printReport();
            pr.Show();
       
        }

        private void salseProductToolStripMenuItem_Click(object sender, EventArgs e)
        {
            SalseOrder so = new SalseOrder();
            so.Show();
        }

        private void allOrderReportsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AllOrderInvoce ali = new AllOrderInvoce();
            ali.Show();
        }

        private void consolidateInvoiceToolStripMenuItem_Click(object sender, EventArgs e)
        {
            consolidetInvoice civ = new consolidetInvoice();
            civ.Show();
        }
    }
}
